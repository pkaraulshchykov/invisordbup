﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Binder;
using Microsoft.Extensions.Configuration.Json;
using System.IO;
using DbUp;
using DbUp.Helpers;

namespace InvisorDBUp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Read config
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var connectionString = config.GetValue<string>("connectionString");
            var scriptsToRunOnceFolder = config.GetValue<string>("scriptsToRunOnceFolder");
            var scriptsToRunRepeatedlyFolder = config.GetValue<string>("scriptsToRunRepeatedlyFolder");
            var logFolder = config.GetValue<string>("logFolder");

            Console.WriteLine(connectionString);
            Console.WriteLine(scriptsToRunOnceFolder);
            Console.WriteLine(scriptsToRunRepeatedlyFolder);
            Console.WriteLine(logFolder);

            // Run once - use journalling

            var upgradeEngine = DeployChanges.To
                .SqlDatabase(connectionString)
                .WithScriptsFromFileSystem(scriptsToRunOnceFolder+"\\9.2.2")
                .WithScriptsFromFileSystem(scriptsToRunOnceFolder+"\\9.2.3")
                .LogToConsole()
                .Build();

            string connectionErrorMessage=string.Empty;

            if (upgradeEngine.TryConnect(out connectionErrorMessage))
            {
                Console.WriteLine("Connection OK");
            } else 
            {
                Console.WriteLine("Connection failed");
                Environment.Exit(1);
            }

            var result = upgradeEngine.PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
            }

            // Run repeatedly - no journalling
            var upgradeEngineRepeatedly = DeployChanges.To
                .SqlDatabase(connectionString)
                .WithScriptsFromFileSystem(scriptsToRunRepeatedlyFolder)
                .JournalTo(new NullJournal())                               // Note NullJournal
                .LogToConsole()
                .Build();

            var resultRepeated = upgradeEngineRepeatedly.PerformUpgrade();

            if (!result.Successful)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(result.Error);
                Console.ResetColor();
            }
 
        }
    }
}
